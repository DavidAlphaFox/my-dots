#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}
keybLayout=$(setxkbmap -v | awk -F "+" '/symbols/ {print $2}')



#start sxhkd to replace Qtile native key-bindings
run sxhkd -c ~/.config/qtile/sxhkd/sxhkdrc &


#starting utility applications at boot time
run xfce4-power-manager &
picom --config $HOME/.config/qtile/picom.conf --vsync --experimental-backends &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &

#starting user applications at boot time
nitrogen --restore &

