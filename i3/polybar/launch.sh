#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2
echo "---" | tee -a /tmp/bar.log 
polybar -c $HOME/myrepo/i3/polybar/config bar | tee -a /tmp/bar.log & disown

echo "Bars launched...">
