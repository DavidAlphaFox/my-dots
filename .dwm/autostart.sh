#!/bin/sh

# compositor
killall picom
while pgrep -u $UID -x picom >/dev/null; do sleep 1; done
picom --config ~/.config/picom/picom.conf --experimental-backends --vsync &
/usr/bin/emacs --daemon &

#bg
#nitrogen --restore &
~/.fehbg &
#wal -R
clipmenud &
dunst &

#setxkbmap -layout colemak &

[ ! -s ~/.config/mpd/pid ] && mpd &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &


#sxhkd
sxhkd -c ~/.dwm/sxhkd/sxhkdrc &


#dwmblocks &
exec slstatus

